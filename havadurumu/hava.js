$(document).ready(function() {
	var api_url = "http://api.wunderground.com/api/83e64b66dcae878a/conditions/q/CA/",
		$fetch = $('.fetch'),
		$logo = $('.logo'),
		cities = ["Düzce", "Osmaniye", "Karabuk", "Yalova", "Igdir", "Ardahan", "Bartin", "Sirnak", "Batman", "Kirikkale", "Karaman", "Bayburt", "Aksaray", "Zonguldak", "Yozgat", "Van", "Usak", "Sanliurfa", "Tunceli", "Trabzon", "Tokat", "Tekirdag", "Sivas", "Sinop", "Siirt", "Samsun", "Sakarya", "Rize", "Ordu", "Nigde", "Nevşehir", "Mus", "Mugla", "Mardin", "Kahramanmaras", "Manisa", "Malatya", "Kütahya", "Konya", "Kocaeli", "Kirsehir", "Kirklareli", "Kayseri", "Kastamonu", "Kars", "Izmir", "Istanbul", "Mersin", "Isparta", "Hatay", "Hakkari", "Gümüshane", "Giresun", "Gaziantep", "Eskisehir", "Erzurum", "Erzincan", "Elazig", "Edirne", "Diyarbakir", "Denizli", "Çorum", "Cankiri", "Canakkale", "Bursa", "Burdur", "Bolu", "Bitlis", "Bingol", "Bilecik", "Balikesir", "Aydin", "Artvin", "Antalya", "Ankara", "Amasya", "Agri", "Afyonkarahisar", "Adiyaman", "Adana"];
		cities = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: $.map(cities,function(city) {
			return {value: city};})
	});

	cities.initialize();
	$('.iller').typeahead({
		hint: true,
		minLength: 1,
		highlight: true
	},
	{
		name :'cities',
		displayKey :'value',
		source: cities.ttAdapter()
	});

	$('.iller').on('keypress', function () {
		$fetch.addClass('showfetch');
		$('.weatherbox').slideUp('slow');
	});
	$fetch.on('click', function() {
		var selected_url = $('.tt-input').val(),
			target_url = api_url + selected_url + '.json' ;

		var request = $.ajax({
			url: target_url,
			async: 'false',
			type: 'GET',
			dataType: 'JSONP',
		});

		request.always(function(json){
			console.log("alwaysMessage");
		});

		request.success(function(json){
			if (json.response.error || $('.tt-input').val() == ""){
				alert("Please type a valid city!");
				return;
			}
			if (json.response.results){
				alert("The city you typed is not exist in our database. Please type an another city!");
			    return;
			}
			else {
				var forecast = json.current_observation,
				locations = forecast.display_location,
				temp = forecast.temp_c;
				$logo.html('<img src='+forecast.image.url+'>');
				$('.city').html(
					'<h2>'+locations.full+'</h2>');
				$('.coordinates').html(
					'<ul><li><b>Latitude:</b> '+locations.latitude+'</li>'+
					'<li><b>Longitude:</b> '+locations.longitude+'</li></ul>'+
					'<ul><li><b>Elevation:</b> '+locations.elevation+'</li>'+
					'<li>Zip: '+locations.zip+'</li></ul>');
				$('.temperature').html('<h1>'+temp+'&#x2103</h1>');
				$('.others').html('<ul><li><b>Wind</b>: '+forecast.wind_string+'</li>'+
					'<li><b>Humidity</b>: '+forecast.relative_humidity+'</li>'+
					'<li><b>UV Index</b>: '+forecast.UV+'</li>'+
					'<li><b>Feels Like</b>: '+forecast.feelslike_c+'&#x2103</li></ul>');
				$('.weatherbox').slideDown('slow', function(){
					if (temp<25)
						$('.temperature h1').addClass('blue');
					else
						$('.temperature h1').addClass('red');
				});
			};
			$fetch.removeClass('showfetch');
		});
		request.fail(function(){
			console.log("error");
		});
	});
});



